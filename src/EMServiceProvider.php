<?php

namespace Pilyavskiy\EM;

use Illuminate\Support\ServiceProvider;

class EMServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(
            realpath(__DIR__.'/../../config/ecommerce-modules.php'), 'ecommerce-modules'
        );

        $this->publishes([
            __DIR__.'/../Modules' => app_path('Modules'),
            __DIR__.'/../../config' => config_path()
        ]);
    }
}
