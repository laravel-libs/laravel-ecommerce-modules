<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Modules namespace
    |--------------------------------------------------------------------------
    |
    | Place where modules will be placed after publish
    |
    */
    'namespace' => 'App\Modules'
    
];
